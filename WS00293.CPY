000100 01  WS00293.                                                     00010000
000200     05  FILE-STATUS                       PIC X(2)               00020000
000300                                               VALUE SPACE.       00030000
000400         88  FILE-OK                           VALUE '00'.        00040000
000500         88  LENGTH-ERROR                      VALUE '04'.        00050000
000600         88  OPTIONAL-FILE-NOT-FOUND           VALUE '05'.        00060000
000700         88  INVALID-OPEN-CLOSE                VALUE '07'.        00070000
000800         88  FILE-AT-END                       VALUE '10'.        00080000
000900         88  SEQUENCE-ERROR                    VALUE '21'.        00090000
001000         88  PRIMARY-DUPLICATE-KEY-FOUND       VALUE '22'.        00100001
001010         88  NO-RECORD-FOUND                   VALUE '23'.        00101001
001100         88  NO-SPACE-AVAILABLE                VALUE '24' '34'.   00110000
001200         88  PERMANENT-IO-ERROR                VALUE '30'.        00120000
001300         88  FILE-NOT-FOUND                    VALUE '35'.        00130000
001400         88  INVALID-OPEN-MODE                 VALUE '37'.        00140000
001500         88  OPEN-ON-LOCKED-FILE               VALUE '38'.        00150000
001600         88  CONFLICTING-ATTRIBUTES            VALUE '39'.        00160000
001700         88  FILE-ALREADY-OPEN                 VALUE '41'.        00170000
001800         88  FILE-ALREADY-CLOSED               VALUE '42'.        00180000
001900         88  REWRITE-ERROR-NO-PREV-READ        VALUE '43'.        00190000
002000         88  DELETE-ERROR-NO-PREV-READ         VALUE '43'.        00200000
002100         88  WRITE-ERROR-RECORD-SIZE           VALUE '44'.        00210000
002200         88  SEQ-READ-AFTER-EOF                VALUE '46'.        00220000
002300         88  ERROR-NOT-OPEN-FOR-INPUT          VALUE '47'.        00230000
002400         88  ERROR-NOT-OPEN-FOR-OUTPUT         VALUE '48'.        00240000
002500         88  ERROR-NOT-OPEN-FOR-IO             VALUE '49'.        00250000
002600         88  QSAM-LOGIC-ERROR                  VALUE '90'.        00260000
002700         88  PASSWORD-FAILURE                  VALUE '91'.        00270000
002800         88  FILE-ACCESS-ERROR                 VALUE '92'.        00280000
002900         88  RESOURCE-NOT-AVAILABLE            VALUE '93'.        00290000
003000         88  FILE-INFO-MISSING                 VALUE '95'.        00300000
003100         88  MISSING-DD-CARD                   VALUE '96'.        00310000
003200         88  FILE-NOT-CLOSED                   VALUE '97'.        00320000